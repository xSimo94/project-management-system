package employee;
 
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
@WebServlet(name="EmployeeServlet", urlPatterns={"/employee"})
public class EmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    // Injected DAO EJB:
    @EJB EmployeeDao employeeDao;

    @Override
    protected void doGet(
        HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        // Display the list of employees:
        request.setAttribute("employees", employeeDao.getAllEmployees());
        request.getRequestDispatcher("/employee.jsp").forward(request, response);
    }

    @Override
    protected void doPost(
        HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Handle a new employee:
        String name = request.getParameter("name");
        if (name != null)
            employeeDao.persist(new Employee(name));

        // Display the list of employees:
        doGet(request, response);
    }
}